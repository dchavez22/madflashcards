package cs.mad.flashcards.adapters;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u001aB\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u000e\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\bJ \u0010\f\u001a\u00020\n2\u0006\u0010\r\u001a\u00020\u00022\u0006\u0010\u000e\u001a\u00020\b2\u0006\u0010\u000f\u001a\u00020\u0010H\u0002J\b\u0010\u0011\u001a\u00020\u0010H\u0016J\u0018\u0010\u0012\u001a\u00020\n2\u0006\u0010\r\u001a\u00020\u00022\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u0018\u0010\u0013\u001a\u00020\u00022\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0010H\u0016J\u0016\u0010\u0017\u001a\u00020\n2\u000e\u0010\u0018\u001a\n\u0012\u0004\u0012\u00020\b\u0018\u00010\u0019R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001b"}, d2 = {"Lcs/mad/flashcards/adapters/FlashcardSetDetailAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcs/mad/flashcards/adapters/FlashcardSetDetailAdapter$ViewHolder;", "cardsDao", "Lcs/mad/flashcards/entities/FlashcardDao;", "(Lcs/mad/flashcards/entities/FlashcardDao;)V", "data", "", "Lcs/mad/flashcards/entities/Flashcard;", "addItem", "", "it", "createCustomDialogue", "viewHolder", "item", "position", "", "getItemCount", "onBindViewHolder", "onCreateViewHolder", "viewGroup", "Landroid/view/ViewGroup;", "viewType", "update", "list", "", "ViewHolder", "app_debug"})
public final class FlashcardSetDetailAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<cs.mad.flashcards.adapters.FlashcardSetDetailAdapter.ViewHolder> {
    private final cs.mad.flashcards.entities.FlashcardDao cardsDao = null;
    private final java.util.List<cs.mad.flashcards.entities.Flashcard> data = null;
    
    public FlashcardSetDetailAdapter(@org.jetbrains.annotations.NotNull()
    cs.mad.flashcards.entities.FlashcardDao cardsDao) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public cs.mad.flashcards.adapters.FlashcardSetDetailAdapter.ViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup viewGroup, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    cs.mad.flashcards.adapters.FlashcardSetDetailAdapter.ViewHolder viewHolder, int position) {
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    public final void addItem(@org.jetbrains.annotations.NotNull()
    cs.mad.flashcards.entities.Flashcard it) {
    }
    
    public final void update(@org.jetbrains.annotations.Nullable()
    java.util.List<cs.mad.flashcards.entities.Flashcard> list) {
    }
    
    private final void createCustomDialogue(cs.mad.flashcards.adapters.FlashcardSetDetailAdapter.ViewHolder viewHolder, cs.mad.flashcards.entities.Flashcard item, int position) {
    }
    
    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007\u00a8\u0006\b"}, d2 = {"Lcs/mad/flashcards/adapters/FlashcardSetDetailAdapter$ViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "bind", "Lcs/mad/flashcards/databinding/ItemFlashcardBinding;", "(Lcs/mad/flashcards/databinding/ItemFlashcardBinding;)V", "binding", "getBinding", "()Lcs/mad/flashcards/databinding/ItemFlashcardBinding;", "app_debug"})
    public static final class ViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        @org.jetbrains.annotations.NotNull()
        private final cs.mad.flashcards.databinding.ItemFlashcardBinding binding = null;
        
        public ViewHolder(@org.jetbrains.annotations.NotNull()
        cs.mad.flashcards.databinding.ItemFlashcardBinding bind) {
            super(null);
        }
        
        @org.jetbrains.annotations.NotNull()
        public final cs.mad.flashcards.databinding.ItemFlashcardBinding getBinding() {
            return null;
        }
    }
}