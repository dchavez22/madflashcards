package cs.mad.flashcards.activities


import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.room.Room
import com.google.android.material.snackbar.Snackbar
import cs.mad.flashcards.adapters.FlashcardSetAdapter
import cs.mad.flashcards.adapters.FlashcardSetDetailAdapter
import cs.mad.flashcards.databinding.ActivityFlashcardSetDetailBinding
import cs.mad.flashcards.entities.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking


class FlashcardSetDetailActivity : AppCompatActivity() {
    private lateinit var binding: ActivityFlashcardSetDetailBinding
    private lateinit var prefs: SharedPreferences
    private lateinit var cardsDao: FlashcardDao
    private var itemsAdded = 11
    private var idIncrement = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFlashcardSetDetailBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        val db = Room.databaseBuilder(
            applicationContext,
            FlashcardAppDatabase::class.java, FlashcardAppDatabase.databaseName
        ).build()

        cardsDao = db.cardsDao()

        prefs = getSharedPreferences("appcards", MODE_PRIVATE)


        binding.flashcardList.adapter = FlashcardSetDetailAdapter(cardsDao)

        loadData()
        //clearStorage()

        binding.addFlashcardButton.setOnClickListener {
            runOnIODetails { cardsDao.insert(Flashcard(idIncrement.toLong(), "Term $itemsAdded", "def $itemsAdded")) }
            (binding.flashcardList.adapter as FlashcardSetDetailAdapter).addItem(
                Flashcard(
                    idIncrement.toLong(),
                    "term $itemsAdded",
                    "def $itemsAdded"
                )
            )
            binding.flashcardList.smoothScrollToPosition((binding.flashcardList.adapter as FlashcardSetDetailAdapter).itemCount - 1)
            idIncrement++
            itemsAdded++
        }

//        binding.studySetButton.setOnClickListener{
//            startActivity(Intent(this, StudySetActivity::class.java))
//
//        }
        binding.deleteSetButton.setOnClickListener {
            finish()

        }
    }

    private fun clearStorage() {
        runOnIO {
            cardsDao.deleteAll()
            prefs.edit().putBoolean("isDownloaded", false).apply()
        }
    }

    private fun loadData() {
        if (prefs.getBoolean("isDownloaded", true)) {
            runOnIODetails {
                var hardcoded = mutableListOf<Flashcard>()
                for (i in 1..10) {
                    cardsDao.insert(Flashcard(idIncrement.toLong(), "Term $i", "Def $i"))
                    hardcoded.add(Flashcard(idIncrement.toLong(), "Term $i", "Def $i"))
                    idIncrement++
                }

                (binding.flashcardList.adapter as FlashcardSetDetailAdapter).update(hardcoded)
            }
            prefs.edit().putBoolean("isDownloaded", false).apply()

        }else{
            runOnIODetails {
                (binding.flashcardList.adapter as FlashcardSetDetailAdapter).update(cardsDao.getAll())
            }
        }


    }
}
fun runOnIODetails(lambda: suspend () -> Unit) {
    runBlocking {
        launch(Dispatchers.IO) { lambda() }
    }
}