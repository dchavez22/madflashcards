package cs.mad.flashcards.activities


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import cs.mad.flashcards.adapters.FlashcardSetAdapter
import cs.mad.flashcards.adapters.FlashcardSetDetailAdapter
import cs.mad.flashcards.entities.FlashcardSet
import cs.mad.flashcards.databinding.ActivityMainBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import android.content.SharedPreferences
import android.util.Log
import androidx.room.Room
import cs.mad.flashcards.entities.FlashcardAppDatabase
import cs.mad.flashcards.entities.FlashcardSetDao
import cs.mad.flashcards.entities.FlashcardSetsContainer

/*
===================================================================================================================

     Reference documentation for recyclers: https://developer.android.com/guide/topics/ui/layout/recyclerview

===================================================================================================================
 */

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var prefs: SharedPreferences
    private lateinit var setsDao: FlashcardSetDao
    private var itemsAdded = 11
    private var idIncrement = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val db = Room.databaseBuilder(
            applicationContext,
            FlashcardAppDatabase::class.java, FlashcardAppDatabase.databaseName
        ).build()

        setsDao = db.setsDao()

        prefs = getSharedPreferences("appsets", MODE_PRIVATE)


        binding.flashcardSetList.adapter = FlashcardSetAdapter(setsDao)


       // clearStorage()
       loadData()

        binding.newSetButton.setOnClickListener {
            runOnIO { setsDao.insert(FlashcardSet(idIncrement.toLong(), "Set $itemsAdded")) }
            (binding.flashcardSetList.adapter as FlashcardSetAdapter).addItem(
                FlashcardSet(
                    idIncrement.toLong(),
                    "Set $itemsAdded"
                )
            )
            binding.flashcardSetList.smoothScrollToPosition((binding.flashcardSetList.adapter as FlashcardSetAdapter).itemCount - 1)
            idIncrement++
            itemsAdded++
        }


    }

    private fun clearStorage() {
        runOnIO {

            prefs.edit().putBoolean("isSetDownloaded", false).apply()
            setsDao.deleteAll()
        }
    }

    private fun loadData() {
        if (prefs.getBoolean("isSetDownloaded", true)) {
            runOnIO {
                var hardcoded = mutableListOf<FlashcardSet>()
                for (i in 1..10) {
                    setsDao.insert(FlashcardSet(idIncrement.toLong(), "set $i"))
                    hardcoded.add(FlashcardSet(idIncrement.toLong(), "set $i"))
                    idIncrement++
                }
                (binding.flashcardSetList.adapter as FlashcardSetAdapter).update(hardcoded)
            }
            prefs.edit().putBoolean("isSetDownloaded", false).apply()

        }else{
            runOnIO {
                (binding.flashcardSetList.adapter as FlashcardSetAdapter).update(setsDao.getAll())
            }
        }
    }
}


fun runOnIO(lambda: suspend () -> Unit) {
    runBlocking {
        launch(Dispatchers.IO) { lambda() }
    }
}



