//package cs.mad.flashcards.activities
//
//import androidx.appcompat.app.AppCompatActivity
//import android.os.Bundle
//import cs.mad.flashcards.adapters.FlashcardSetDetailAdapter
//import cs.mad.flashcards.databinding.ActivityStudySetBinding
//import cs.mad.flashcards.entities.Flashcard
//
//class StudySetActivity : AppCompatActivity() {
//
//    private lateinit var binding: ActivityStudySetBinding
//    private val flashcards = Flashcard.getHardcodedFlashcards().toMutableList()
//    private val missedCards = mutableListOf<Flashcard>()
//    private val initialCount = flashcards.size
//    private var completedCount = 0
//    private val missedCount
//        get() = missedCards.size
//    private var correctCount = 0
//    private var isCardFlipped = false
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        binding = ActivityStudySetBinding.inflate(layoutInflater)
//        setContentView(binding.root)
//
//        setupButtons()
//        binding.cardView.setOnClickListener{
//            flipCard()
//        }
//
//    }
//
//    private fun missCurrent() {
//        val current = flashcards.removeFirst()
//        missedCards.add(current)
//        flashcards.add(current)
//        updateMissedText()
//        isCardFlipped = false
//        updateCard()
//    }
//
//    private fun skipCurrent() {
//        val current = flashcards.removeFirst()
//        flashcards.add(current)
//        isCardFlipped = false
//        updateCard()
//    }
//
//    private fun markCorrectCurrent() {
//        val current = flashcards.removeFirst()
//        completedCount += 1
//        updateCompletedText()
//
//        if (!missedCards.contains(current)) {
//            correctCount += 1
//            updateCorrectText()
//        }
//        if (flashcards.size == 0) {
//            binding.showFlashcard.text = "Finished!"
//            binding.missedFlashcardButton.isEnabled = false
//            binding.skipFlashcardButton.isEnabled = false
//            binding.correctFlashcardButton.isEnabled = false
//            binding.cardView.setOnClickListener {  }
//        } else {
//            isCardFlipped = false
//            updateCard()
//        }
//    }
//
//    private fun flipCard() {
//        if (flashcards.isNotEmpty()) {
//            isCardFlipped = !isCardFlipped
//            updateCard()
//        }
//    }
//
//    private fun updateCard() {
//        if (isCardFlipped) {
//            binding.showFlashcard.text = flashcards[0].definition
//        } else {
//            binding.showFlashcard.text = flashcards[0].term
//        }
//    }
//    private fun updateCompletedText() {
//        binding.flashcardCount.text = "$completedCount / $initialCount"
//    }
//
//    private fun updateMissedText() {
//        binding.missedCount.text = "Missed: $missedCount"
//    }
//
//    private fun updateCorrectText() {
//        binding.correctCount.text = "Correct: $correctCount"
//    }
//
//    private fun setupButtons() {
//        binding.exitStudy.setOnClickListener { finish() }
//        binding.missedFlashcardButton.setOnClickListener { missCurrent() }
//        binding.skipFlashcardButton.setOnClickListener { skipCurrent() }
//        binding.correctFlashcardButton.setOnClickListener { markCorrectCurrent() }
//    }
//
//}