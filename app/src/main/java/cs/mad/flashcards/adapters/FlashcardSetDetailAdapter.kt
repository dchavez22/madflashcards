package cs.mad.flashcards.adapters

import android.content.DialogInterface
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import cs.mad.flashcards.R
import cs.mad.flashcards.activities.FlashcardSetDetailActivity
import cs.mad.flashcards.activities.runOnIO
import cs.mad.flashcards.activities.runOnIODetails
import cs.mad.flashcards.databinding.ItemFlashcardBinding
import cs.mad.flashcards.databinding.ItemFlashcardSetBinding
import cs.mad.flashcards.entities.Flashcard
import cs.mad.flashcards.entities.FlashcardDao
import cs.mad.flashcards.entities.FlashcardSet
import cs.mad.flashcards.entities.FlashcardSetDao

class FlashcardSetDetailAdapter(private val cardsDao: FlashcardDao) :
    RecyclerView.Adapter<FlashcardSetDetailAdapter.ViewHolder>(){

        //reference to data set
        private val data = mutableListOf<Flashcard>()

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    class ViewHolder(bind: ItemFlashcardBinding) : RecyclerView.ViewHolder(bind.root) {
        // store view references as properties using findViewById on view
        val binding: ItemFlashcardBinding = bind


    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        /*
            this is written for you but understand what is happening here
            the layout for an individual item is being inflated
            the inflated layout is passed to view holder for storage

            THE ITEM LAYOUT STILL NEEDS TO BE SETUP IN THE LAYOUT EDITOR
         */
        return ViewHolder(
            ItemFlashcardBinding.inflate(
                LayoutInflater.from(
                    viewGroup.context
                ), viewGroup, false
            )
        )

    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        /*
            fill the contents of the view using references in view holder and current position in data set

         */
        val item = data[position]
        viewHolder.binding.flashcardTitle.text = item.term
        viewHolder.binding.root.setOnClickListener {
            AlertDialog.Builder(viewHolder.itemView.context)
                .setTitle(item.term)
                .setMessage(item.definition)
                .setPositiveButton("close") { dialogInterface: DialogInterface, i: Int ->
                    // do something on click
                }
                .create()
                .show()
        }

        viewHolder.binding.root.setOnLongClickListener {
            createCustomDialogue(viewHolder, item, position)
            true

        }


    }


    override fun getItemCount(): Int {
        // return the size of the data set
        return data.size
    }

    fun addItem(it: Flashcard) {
        data.add(it)
        notifyItemInserted(data.lastIndex)
    }

    fun update(list: List<Flashcard>?) {
        list?.let {

            data.addAll(it)
            notifyDataSetChanged()
        }
    }

    private fun createCustomDialogue(viewHolder: ViewHolder, item: Flashcard, position: Int){
            val titleView = LayoutInflater.from(viewHolder.itemView.context).inflate(R.layout.dialogue_title, null)
            val bodyView = LayoutInflater.from(viewHolder.itemView.context).inflate(R.layout.dialogue_body, null)
            val titleEditText = titleView.findViewById<EditText>(R.id.dialogue_title)
            val bodyEditText = bodyView.findViewById<EditText>(R.id.dialogue_body)
            titleEditText.setText(item.term)
            bodyEditText.setText(item.definition)

            AlertDialog.Builder(viewHolder.itemView.context)
                .setCustomTitle(titleView)
                .setView(bodyView)
                .setPositiveButton("save") { dialogInterface: DialogInterface, i: Int ->
                    // do something on click
                    Snackbar.make(viewHolder.binding.root, titleEditText.text.toString(), Snackbar.LENGTH_LONG).show()

                    //data[position] = Flashcard(item.id,titleEditText.text.toString(), bodyEditText.text.toString())
                    item.term = titleEditText.text.toString()
                    item.definition = bodyEditText.text.toString()
                    runOnIODetails { cardsDao.update(item)}
                    notifyItemChanged(position)
                }
                .setNegativeButton("delete"){dialogInterface: DialogInterface, i: Int ->
                    // do something on click
                    runOnIODetails { cardsDao.delete(item)}
                    data.removeAt(position)
                    notifyItemRemoved(position)

                    Snackbar.make(viewHolder.binding.root, "deleted successfully", Snackbar.LENGTH_LONG).show()

                }
                .create()
                .show()

        }




}