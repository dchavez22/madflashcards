package cs.mad.flashcards.adapters



import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import cs.mad.flashcards.activities.FlashcardSetDetailActivity
import cs.mad.flashcards.databinding.ItemFlashcardSetBinding
import cs.mad.flashcards.entities.FlashcardSet
import cs.mad.flashcards.entities.FlashcardSetDao

class FlashcardSetAdapter(private val setsDao: FlashcardSetDao) :
    RecyclerView.Adapter<FlashcardSetAdapter.ViewHolder>() {

    // need to get and store a reference to the data set
    private val data = mutableListOf<FlashcardSet>()


    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    class ViewHolder(bind: ItemFlashcardSetBinding) : RecyclerView.ViewHolder(bind.root){
        // store view references as properties using findViewById on view

        val binding: ItemFlashcardSetBinding = bind


        }


    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        /*
            this is written for you but understand what is happening here
            the layout for an individual item is being inflated
            the inflated layout is passed to view holder for storage

            THE ITEM LAYOUT STILL NEEDS TO BE SETUP IN THE LAYOUT EDITOR
         */
        return ViewHolder(ItemFlashcardSetBinding.inflate(LayoutInflater.from(viewGroup.context), viewGroup, false))

    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        /*
            fill the contents of the view using references in view holder and current position in data set
            to launch FlashcardSetDetailActivity ->

         */
        val item = data[position]
        viewHolder.binding.flashcardSetText.text = item.title
        viewHolder.binding.root.setOnClickListener {
            viewHolder.itemView.context.startActivity(Intent(viewHolder.itemView.context, FlashcardSetDetailActivity::class.java))
        }

    }

    override fun getItemCount(): Int {
        // return the size of the data set
        return data.size
    }

    fun addItem(it: FlashcardSet){
        data.add(it)
        notifyItemInserted(data.lastIndex)
    }

    fun update(list: List<FlashcardSet>?) {
        list?.let {

            data.addAll(it)
            notifyDataSetChanged()
        }
    }

}