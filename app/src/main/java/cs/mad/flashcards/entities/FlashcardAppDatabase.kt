package cs.mad.flashcards.entities

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [FlashcardSet::class, Flashcard::class], version = 1)
abstract class FlashcardAppDatabase: RoomDatabase() {
    companion object {
        const val databaseName = "FLASHCARD_APP_DATABASE"
    }

    abstract fun cardsDao() : FlashcardDao
    abstract fun setsDao() : FlashcardSetDao
}