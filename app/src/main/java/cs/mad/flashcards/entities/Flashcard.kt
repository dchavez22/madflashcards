package cs.mad.flashcards.entities


import androidx.room.*

@Entity
data class Flashcard(
    @PrimaryKey val id: Long,
    var term: String,
    var definition: String
)

@Dao
interface FlashcardDao {
    @Query("select * from flashcard")
    suspend fun getAll(): List<Flashcard>

    @Insert
    suspend fun insert(card: Flashcard)

    @Insert
    suspend fun insertAll(cards: List<Flashcard>)

    @Update
    suspend fun update(card: Flashcard)

    @Delete
    suspend fun delete(card: Flashcard)

    @Query("delete from flashcard")
    suspend fun deleteAll()

}

