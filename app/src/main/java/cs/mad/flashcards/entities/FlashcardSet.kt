package cs.mad.flashcards.entities

import androidx.room.*

data class FlashcardSetsContainer(
    val FlashcardSets: List<FlashcardSet>
)

@Entity
data class FlashcardSet(
    @PrimaryKey val id: Long,
    var title: String)

@Dao
interface FlashcardSetDao {
    @Query("select * from flashcardset")
    suspend fun getAll(): List<FlashcardSet>

    @Insert
    suspend fun insert(set: FlashcardSet)

    @Insert
    suspend fun insertAll(sets: List<FlashcardSet>)

    @Update
    suspend fun update(set: FlashcardSet)

    @Delete
    suspend fun delete(set: FlashcardSet)

    @Query("delete from flashcardset")
    suspend fun deleteAll()

}